<?php

class AdlInterfaceErp{

    public function generarArchivosPlanos($fecha = 'yesterday'){
        $db = new AdlDataBase();
        $ventas = $db->obtenerVentasDia($fecha);
        
        $coleccionImpuestos = [];
        $coleccionTerceros = [];
        $coleccionClientes = [];
        $coleccionVentas = [];
        $coleccionDetalles = [];

        foreach ($ventas as $v){
            if (!array_key_exists($v->cliente->id_customer, $coleccionImpuestos)){
                $impuesto = new stdClass();

                $impuesto->NIT = $v->cliente->cedula;
                $impuesto->SUCURSAL = '001';

                $coleccionImpuestos[$v->cliente->cedula] = $impuesto;
            }

            if (!array_key_exists($v->cliente->cedula, $coleccionTerceros)){
                $tercero = new stdClass();

                $tercero->COD_NIT = $v->cliente->cedula;
                $tercero->NIT = $v->cliente->cedula;
                $tercero->APELLIDO1 = $v->cliente->lastname;
                $tercero->APELLIDO2 = '';
                $tercero->NOMBRES = $v->cliente->firstname;
                $tercero->CONTACTO = $v->cliente->firstname;
                $tercero->DIRECCION_1 = $v->direccion->address1.' '.$v->direccion->address2;
                $tercero->DEPTO = $v->direccion->id_departamento;
                $tercero->CIUDAD = $v->direccion->id_ciudad;
                $tercero->BARRIO = $v->direccion->other;
                $tercero->TELEFONO = !empty($v->direccion->phone) ? $v->direccion->phone : $v->direccion->phone_mobile;
                $tercero->EMAIL = $v->cliente->email;
                $tercero->FECHA_NAC = date('Ymd', strtotime($v->cliente->birthday));
                $tercero->CELULAR = !empty($v->direccion->phone_mobile) ? $v->direccion->phone_mobile : $v->direccion->phone;

                $coleccionTerceros[$v->cliente->cedula] = $tercero;
            }

            if (!array_key_exists($v->cliente->cedula, $coleccionClientes)){
                $cliente = new stdClass();

                $cliente->NIT_CLIENTE = $v->cliente->cedula;
                $cliente->SUC_CLIENTE = '001';
                $cliente->RAZ_CLIENTE = $v->cliente->firstname.' '.$v->cliente->lastname;
                $cliente->LISTA_PRECIOS = 100;
                $cliente->CONTACTO = $v->cliente->firstname;
                $cliente->DIRECCION_1 = $v->direccion->address1.' '.$v->direccion->address2;
                $cliente->DEPARTAMENTO = $v->direccion->id_departamento;
                $cliente->CIUDAD = $v->direccion->id_ciudad;
                $cliente->BARRIO = $v->direccion->other;
                $cliente->TELEFONO = !empty($v->direccion->phone) ? $v->direccion->phone : $v->direccion->phone_mobile;
                $cliente->EMAIL = $v->cliente->email;
                $cliente->FECHA_INGRESO = date('Ymd');

                $coleccionClientes[$v->cliente->cedula] = $cliente;
            }

            $pedido = new stdClass();

            $pedido->CONSECUTIVO = $v->id_order;
            $pedido->FECHA_DOCUMENTO = date('Ymd', strtotime(substr($v->date_add, 0, 10)));
            $pedido->TERCERO_FACTURAR = $v->cliente->cedula;
            $pedido->TERCERO_DESPACHAR = $v->cliente->cedula;
            $pedido->FECHA_ENTREGA = date('Ymd', strtotime(substr($v->date_add, 0, 10)));
            $pedido->OBSERVACIONES = "Pedido generado por computador";
            $pedido->CONTACTO = $v->cliente->firstname;
            $pedido->DIRECCION = $v->direccion->address1.' '.$v->direccion->address2;
            $pedido->DEPARTAMENTO = $v->direccion->id_departamento;
            $pedido->CIUDAD = $v->direccion->id_ciudad;
            $pedido->TELEFONO = !empty($v->direccion->phone) ? $v->direccion->phone : $v->direccion->phone_mobile;

            $coleccionVentas[] = $pedido;
            
            foreach ($v->detalles as $d){
                $detalle = new stdClass();

                $detalle->CONSECUTIVO = $d->id_order;
                $detalle->REGISTRO = $d->id_order_detail;
                $detalle->ITEM = $d->producto->reference;
                $detalle->BODEGA = '01001';
                $detalle->FECHA_PEDIDO = date('Ymd', strtotime($v->invoice_date));
                $detalle->CANTIDAD = str_pad($d->product_quantity, 15, '0', STR_PAD_LEFT).'.0000';
                $detalle->PRECIO = str_pad($d->unit_price_tax_incl, 15, '0', STR_PAD_LEFT).'.0000';
                $detalle->DESCRIPCION = $d->product_name;
            }

            $coleccionDetalles[] = $detalle;
        }

        $xml = new AdlXmlSerializador();
        
        $ret = [
            "impuestos" => $xml->serializarColeccion($coleccionImpuestos, 'Impuestos'),
            "terceros" => $xml->serializarColeccion($coleccionTerceros, 'Terceros'),
            "clientes" => $xml->serializarColeccion($coleccionClientes, 'Clientes'),
            "ventas" => $xml->serializarColeccion($coleccionVentas, 'Pedidos', $coleccionDetalles, 'Mov_Pedidos'),
        ];
        print_r($ret);
        return $ret;
    }

    public function procesarLoteDia($fecha = 'yesterday'){
        try{
            $db = new AdlDataBase();
            $lote = $db->obtenerLoteDia($fecha);
            if (sizeof($lote)){
                throw new Exception('Lote ya procesado');
            }

            $planos = $this->generarArchivosPlanos($fecha);
            
            $ws = new SoapClient('http://190.85.126.222:3392/GTIntegration/ServiciosWeb/wsGenerarPlano.asmx');
            $ruta = 'C:\\inetpub\\wwwroot\\GTIntegration\\Planos\\';
            
            $rutaFinal = $ruta;
            $resTerceros = $ws->ImportarDatosXML(29983, 'Terceros', 2, 1, 'gt', 'gt', $planos['terceros'], $rutaFinal);
            print_r($resTerceros);

            $rutaFinal = $ruta;
            $resClientes = $ws->ImportarDatosXML(29881, 'Clientes', 2, 1, 'gt', 'gt', $planos['clientes'], $rutaFinal);
            print_r($resClientes);        

            $rutaFinal = $ruta;
            $resImpuestos = $ws->ImportarDatosXML(30725, 'Unificacion Terceros', 2, 1, 'gt', 'gt', $planos['impuestos'], $rutaFinal);
            print_r($resImpuestos);       

            $rutaFinal = $ruta;
            $resVentas = $ws->ImportarDatosXML(30357, 'Pedidos', 2, 1, 'gt', 'gt', $planos['ventas'], $rutaFinal);
            print_r($resVentas);

            $lote = new stdClass();
            $lote->fecha = $fecha;
            $lote->planos = json_encode($planos);
            $db->guardarLoteDia($lote);

            $notificador->notificarExitoInterface($fecha);
        }
        catch(Exception $ex){
            $ret = [
                'ok' => 0,
                'mensaje' => $ex->getMessage()
            ];

            $notificador = new AdlNotificador();
            $notificador->notificarErrorInterface($ex, $fecha);

            print_r($ret);
        }

        return $ret;
    }
}