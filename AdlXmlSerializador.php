<?php

class AdlXmlSerializador{

    public function serializarColeccion($coleccion, $base, $coleccion2 = null, $base2 = null){
        $xml = new DOMDocument();
        $nodoRoot = $xml->appendChild($xml->createElement('MyDataSet'));
        
        foreach($coleccion as $item){
            $nodoItem = $nodoRoot->appendChild($xml->createElement($base));
            $campos = get_object_vars($item);
            foreach($campos as $key => $c){
                $nodoItem->appendChild($xml->createElement($key, $c));
            }
        }

        if(sizeof($coleccion2)){
            foreach($coleccion2 as $item){
                $nodoItem = $nodoRoot->appendChild($xml->createElement($base2));
                $campos = get_object_vars($item);
                foreach($campos as $key => $c){
                    $nodoItem->appendChild($xml->createElement($key, $c));
                }
            }
        }

        return $xml->saveXML();
    }
}