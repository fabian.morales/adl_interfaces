alter table ad2014_customer add column cedula varchar(30) after acepta_politicas;

create table ad2014_lote_dia(
    id_lote int not null auto_increment primary key,
    feha date not null unique,
    planos text
)COLLATE='utf8_general_ci' ENGINE=InnoDB;