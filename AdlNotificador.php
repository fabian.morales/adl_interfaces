<?php

class AdlNotificador{
    public function notificarErrorInterface($mensaje, $fecha){
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        try {
            
            $mail->isSMTP();
            $mail->Host = '';
            $mail->SMTPAuth = true;
            $mail->Username = '';
            $mail->Password = '';
            $mail->SMTPSecure = '';
            $mail->Port =  '';

            $mail->setFrom('correo@demo.com', 'ADL Interfaces');
            $mail->addAddress('destino@demo.com', 'Demo 1');

            $mail->isHTML(true);
            $mail->Subject = 'La interfaz se ha procesado con errores - '.$fecha;
            $mail->Body    = '<p>La interfaz del día '.$fecha.'se ha procesado con los siguientes errores:</p>'.$mensaje;
            $mail->AltBody = 'La interfaz del día '.$fecha.'se ha procesado con los siguientes errores:'.$mensaje;

            $mail->send();
            
        } catch (Exception $e) {
            
        }
    }

    public function notificarExitoInterface($fecha){
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        try {
            
            $mail->isSMTP();
            $mail->Host = '';
            $mail->SMTPAuth = true;
            $mail->Username = '';
            $mail->Password = '';
            $mail->SMTPSecure = '';
            $mail->Port =  '';

            $mail->setFrom('correo@demo.com', 'ADL Interfaces');
            $mail->addAddress('destino@demo.com', 'Demo 1');

            $mail->isHTML(true);
            $mail->Subject = 'La interfaz se ha procesado exitosamente - '.$fecha;
            $mail->Body    = '<p>La interfaz del día '.$fecha.'se ha procesado exitosamente</p>';
            $mail->AltBody = 'La interfaz del día '.$fecha.'se ha procesado exitosamente';

            $mail->send();
            
        } catch (Exception $e) {
            
        }
    }
}