<?php

class AdlDataBase{
    var $host = '';
    var $user = '';
    var $password = '';
    var $database = '';

    public function __construct(){
        cargarArchivo('Configuracion_PS');
        
        $this->host = _DB_SERVER_;
        $this->database = _DB_NAME_;
        $this->user = _DB_USER_;
        $this->password = _DB_PASSWD_;
    }

    public function conectar(){
        $conexion = new mysqli($this->host, $this->user, $this->password, $this->database);
        return $conexion;
    }

    public function ejecutarConsulta($sql, $conexion, $key = ''){
        if (!sizeof($conexion)){
            $conexion = $this->conectar();
        }

        $res = $conexion->query($sql);
        $ret = null;

        if ($res && $res->num_rows){
            if (!empty($key)){
                $ret = [];
                while($row = $res->fetch_object()){
                    $ret[$row->$key] = $row;
                }
            }
            else{
                $ret = $res->fetch_all();
            }
        }

        return $ret;
    }

    public function ejecutarSql($sql, $conexion){
        if (!sizeof($conexion)){
            $conexion = $this->conectar();
        }

        $ret = $conexion->query($sql);
        
        return $ret;
    }

    private function array_pluck($array, $key) {
        return array_map(function($v) use ($key) {
          return is_object($v) ? $v->$key : $v[$key];
        }, $array);
    }

    public function obtenerVentasDia($fecha){
        $inicio = date('Y-m-d', strtotime($fecha));
        $fin = date("Y-m-d", strtotime($fecha)).' 23:59:59';
        $conexion = $this->conectar();

        $sqlVenta = 'select * from ad2014_orders where date_add >= \''.$inicio.'\' and date_add <= \''.$fin.'\'';
        $ventas = $this->ejecutarConsulta($sqlVenta, $conexion, 'id_order');

        if (sizeof($ventas)){
            $sqlDetalle = 'select b.* ' .
                'from ad2014_orders a ' .
                'left join ad2014_order_detail b on (a.id_order = b.id_order) '.
                'where a.date_add >= \''.$inicio.'\' and a.date_add <= \''.$fin.'\'';
            $detalles = $this->ejecutarConsulta($sqlDetalle, $conexion, 'id_order_detail');

            $sqlProducto = 'select c.*, b.id_order_detail ' .
                'from ad2014_orders a ' .
                'left join ad2014_order_detail b on (a.id_order = b.id_order) ' .
                'left join ad2014_product c on (b.product_id = c.id_product) ' .
                'where a.date_add >= \''.$inicio.'\' and a.date_add <= \''.$fin.'\'';
            $productos = $this->ejecutarConsulta($sqlProducto, $conexion, 'id_product');

            $sqlCliente = 'select b.*, a.id_order ' .
                'from ad2014_orders a ' .
                'left join ad2014_customer b on (a.id_customer = b.id_customer) ' .
                'where a.date_add >= \''.$inicio.'\' and a.date_add <= \''.$fin.'\' and trim(b.cedula) <> \'\'';
            $clientes = $this->ejecutarConsulta($sqlCliente, $conexion, 'id_customer');

            $sqlDireccion = 'select b.*, a.id_order ' .
                'from ad2014_orders a ' .
                'left join ad2014_address b on (a.id_address_invoice = b.id_address) ' .
                'where a.date_add >= \''.$inicio.'\' and a.date_add <= \''.$fin.'\'';
            $direcciones = $this->ejecutarConsulta($sqlDireccion, $conexion, 'id_address');
            
            foreach ($direcciones as $d){
                $partesCiudad = explode('-', $d->city);
                if (sizeof($partesCiudad) > 1 && strlen($partesCiudad[0]) >= 5){
                    $d->id_departamento = substr($d->city, 0, 2);
                    $d->id_ciudad = substr($d->city, 2, 3);
                }

                $ventas[$d->id_order]->direccion = $d;
            }

            foreach ($clientes as $c){
                $ventas[$c->id_order]->cliente = $c;
            }

            foreach ($productos as $p){
                $detalles[$p->id_order_detail]->producto = $p;
            }

            foreach ($detalles as $d){
                $ventas[$d->id_order]->detalles[] = $d;
            }
            
        }

        return $ventas;
    }

    public function obtenerLoteDia($fecha){
        $sql = 'select * from ad2014_lote_dia where fecha = \''.$inicio.'\' limit 0, 1';
        $lote = $this->ejecutarConsulta($sql, $conexion);

        return $lote;
    }

    public function guardarLoteDia($lote){
        $sql = 'insert into ad2014_lote_dia (fecha, planos) values (\''.$lote->fecha.'\', \''.$lote->planos.'\')';
        $ret = $this->ejecutarSql($sql, $conexion);

        return $ret;
    }
}