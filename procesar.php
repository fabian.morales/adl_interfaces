<?php

define('BASE_DIR', __DIR__);

function cargarArchivo($archivo) {
    $files = [
        'Configuracion_PS' => BASE_DIR . '/../config/settings.inc.php',
        'AdlDataBase' => BASE_DIR . '/AdlDataBase.php',
        'AdlXmlSerializador' => BASE_DIR . '/AdlXmlSerializador.php',
        'AdlNotificador' => BASE_DIR . '/AdlNotificador.php',
        'AdlInterfaceErp' => BASE_DIR . '/AdlInterfaceErp.php',
    ];

    if (!key_exists($archivo, $files)) {
        throw new Exception($archivo . ' no está registrado');
    }

    if (!is_file($files[$archivo])) {
        throw new Exception($files[$archivo] . ' no existe');
    }

    include_once($files[$archivo]);
};

spl_autoload_register('cargarArchivo');

$interface = new AdlInterfaceErp();
$interface->generarArchivosPlanos();
